FROM python:alpine
LABEL maintainer="Hossein Davoodi <mr.hdavoodi@gmail.com>"
RUN apk update && apk add --no-cache git
WORKDIR /flaspi
COPY . .

RUN pip3 install -U pip && \
    pip3 install -r requirements.txt && \
    pip3 freeze
    
EXPOSE 8090
ENTRYPOINT [ "python3" ]
CMD ["app.py"]





