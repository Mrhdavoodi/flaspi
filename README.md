# Flaspi

Python REST APIs With Flask, Connexion, and Later with SQLAlchemy

### Containerising the App
Issue the following command to make a docker image for the app:

	docker build -t flaspi:v1 .

then you can make a container out of the newly created image by issueing:
	 docker run -d -p 0.0.0.0:80:8090 flaspi:v1
### In the Browser
now you just need to type **localhost** in the browser to see the home page which is a very simple html page
by typing http://localhost/api/ui/ in your browser you will see the swagger ui and he you can easily work with the API.

